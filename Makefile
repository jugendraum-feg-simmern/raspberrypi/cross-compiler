# Makefile to automatically build gcc for cross compilation with RPi 3
# based upon work by Paul, see https://solarianprogrammer.com/2018/05/06/building-gcc-cross-compiler-raspberry-pi/

all: clean binutils gcc-and-glibc


# ensure INSTALL_DIR is set and writable
ifndef INSTALL_DIR
$(error please specify INSTALL_DIR)
endif

################################################################################
# VERSIONS AND CONSTANTS
################################################################################
BINUTILS_VER = 2.31
GLIBC_VER = 2.28
GCC_VER = 8.3.0

GNU_FTP = https://ftpmirror.gnu.org
BINUTILS_URL = $(GNU_FTP)/binutils/binutils-$(BINUTILS_VER).tar.gz
GLIBC_URL = $(GNU_FTP)/glibc/glibc-$(GLIBC_VER).tar.gz
GCC_URL = $(GNU_FTP)/gcc/gcc-$(GCC_VER)/gcc-$(GCC_VER).tar.gz
LINUX_GIT = https://github.com/raspberrypi/linux

N_JOBS ?= 4

################################################################################
# provide $(DL) $(SRC) $(BUILD)
################################################################################
DL = download
$(DL):
		mkdir -p $@

SRC = src
$(SRC):
		mkdir -p $@

BUILD = build
$(BUILD):
		mkdir -p $@

################################################################################
# provide $(*_TAR) and $(*_DIR)
################################################################################
# download and extract binutils
BINUTILS_TAR = $(DL)/binutils-$(BINUTILS_VER).tar.gz
$(BINUTILS_TAR): $(DL)
		if [ ! -f $@ ]; then cd $(DL); wget -q $(BINUTILS_URL); fi
BINUTILS_DIR = $(SRC)/binutils-$(BINUTILS_VER)
$(BINUTILS_DIR): $(SRC) $(BINUTILS_TAR)
		mkdir -p $@
		tar -xf $(BINUTILS_TAR) -C $@ --strip-components=1

# download and extract glibc
GLIBC_TAR = $(DL)/glibc-$(GLIBC_VER).tar.gz
$(GLIBC_TAR): $(DL)
		if [ ! -f $@ ]; then cd $(DL); wget -q $(GLIBC_URL); fi
GLIBC_DIR = $(SRC)/glibc-$(GLIBC_VER)
$(GLIBC_DIR): $(SRC) $(GLIBC_TAR)
		mkdir -p $@
		tar -xf $(GLIBC_TAR) -C $@ --strip-components=1

# download and extract gcc
GCC_TAR = $(DL)/gcc-$(GCC_VER).tar.gz
$(GCC_TAR): $(DL)
		if [ ! -f $@ ]; then cd $(DL); wget -q $(GCC_URL); fi
GCC_DIR = $(SRC)/gcc-$(GCC_VER)
$(GCC_DIR): $(SRC) $(GCC_TAR)
		mkdir -p $@
		tar -xf $(GCC_TAR) -C $@ --strip-components=1

# download kernel for pi
LINUX_DIR = $(DL)/linux
$(LINUX_DIR): $(DL)
		if [ ! -d $(LINUX_DIR) ]; then cd $(DL); git clone --depth=1 $(LINUX_GIT); fi

################################################################################
# install kernel headers
################################################################################
.PHONY: kernel-headers
kernel-headers: $(LINUX_DIR) $(INSTALL_DIR)
		# here we decide to buld for RPi 3 or RPi 2 by setting KERNEL=kernel7, use kernel for RPi 1 and RPi Zero, use kernel7l for RPi 4
		cd $(LINUX_DIR); \
			KERNEL=kernel7; \
			make ARCH=arm INSTALL_HDR_PATH=$(INSTALL_DIR)/arm-linux-gnueabihf headers_install

################################################################################
# provide $(BINUTILS)
################################################################################
BINUTILS = $(BUILD)/binutils-$(BINUTILS_VER)
.PHONY: binutils
binutils: kernel-headers $(BINUTILS_DIR) $(BUILD)
		mkdir -p $(BINUTILS)
		cd $(BINUTILS); \
		../../$(BINUTILS_DIR)/configure --prefix=$(INSTALL_DIR) --target=arm-linux-gnueabihf --with-arch=armv6 --with-fpu=vfp --with-float=hard --disable-multilib; \
		make -j $(N_JOBS); \
		make install

################################################################################
# provide $(GCC) and $(GLIBC)
################################################################################
# This must be done in one step since they depend upon each other and must be
# build partially intertwined
GCC = $(BUILD)/gcc-$(GCC_VER)
GLIBC = $(BUILD)/glibc-$(GLIBC_VER)
.PHONY: gcc-and-glibc

gcc-and-glibc: $(GCC_DIR) $(GLIBC_DIR) binutils
		# download prerequisites for gcc
		cd $(GCC_DIR); \
			./contrib/download_prerequisites

		# first part of gcc
		mkdir -p $(GCC)
		cd $(GCC); \
				../../$(GCC_DIR)/configure  --prefix=$(INSTALL_DIR) --target=arm-linux-gnueabihf --enable-languages=c,c++,fortran --with-arch=armv6 --with-fpu=vfp --with-float=hard --disable-multilib; \
				make -j $(N_JOBS) all-gcc; \
				make install-gcc
		# first half of glibc
		mkdir -p $(GLIBC)
		cd $(GLIBC); \
				../../$(GLIBC_DIR)/configure --prefix=$(INSTALL_DIR)/arm-linux-gnueabihf --build=$(MACHTYPE) --host=arm-linux-gnueabihf --target=arm-linux-gnueabihf --with-arch=armv6 --with-fpu=vfp --with-float=hard --with-headers=$(INSTALL_DIR)/arm-linux-gnueabihf/include --disable-multilib libc_cv_forced_unwind=yes; \
				make install-bootstrap-headers=yes install-headers; \
				make -j $(N_JOBS) csu/subdir_lib; \
				install csu/crt1.o csu/crti.o csu/crtn.o $(INSTALL_DIR)/arm-linux-gnueabihf/lib; \
				arm-linux-gnueabihf-gcc -nostdlib -nostartfiles -shared -x c /dev/null -o $(INSTALL_DIR)/arm-linux-gnueabihf/lib/libc.so; \
				mkdir -p $(INSTALL_DIR)/arm-linux-gnueabihf/include/gnu; \
				touch $(INSTALL_DIR)/arm-linux-gnueabihf/include/gnu/stubs.h
		# second part of gcc
		cd $(GCC); \
				make -j $(N_JOBS) all-target-libgcc; \
				make install-target-libgcc
		# second half of glibc
		cd $(GLIBC); \
				make -j $(N_JOBS); \
				make install;
		# third and last part of gcc
		cd $(GCC); \
				make -j $(N_JOBS); \
				make install;

################################################################################
# thoughtful cleanup
################################################################################
# keep downlods cached in standard clean
.PHONY: clean
clean:
		rm -rf $(SRC)
		rm -rf $(BUILD)

# also delete downlodaded files
.PHONY: erase
erase: clean
		rm -rf $(DL)

.PHONY: uninstall
uninstall:
		rm -rf $(INSTALL_DIR)
