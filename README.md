# Create cross compile toolchain

## 1. About
This repo contains a makefile to build a toolchain to cross compile c++ programs for raspberry pi.

## 2. Usage
Install build requirements
```shell
sudo apt install build-essential gawk git texinfo bison file wget gcc make
```
On arch based systems use `base-devel` instead of `build-essential`

TODO: properly use `make` and `sudo make install` to build with non root privileges and install afterwards. Until then:
```shell
sudo mkdir /opt/gcc-8.3.0-rpi3
sudo chown $USER:$USER /opt/gcc-8.3.0-rpi3
export PATH=/opt/gcc-8.3.0-rpi3/bin:$PATH
make INSTALL_DIR=/opt/gcc-8.3.0-rpi3
```
All sources will be downlodad automatically and get cached in `downloads/`. A `make clean` will keep them for reuse, run `make erase` to delete them as well.

## 3. Versions
A fresh install of the Rasperry Pi OS (previously known as raspbian buster) uses the following versions:
```shell
$~ gcc --version
gcc version 8.3.0 (Raspbian 8.3.0-6+rpi1)
$~ ld -v
GNU ld (GNU Binutils for Raspbian) 2.31.1
$~ ldd --version
ldd (Debian GLIBC 2.28-10+rpi1) 2.28
```
One can buld a more recent version of gcc, however the original gcc version should be used to compile glibc. By default, the Makefile uses the above versions.
